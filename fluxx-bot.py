import time
import telepot
import json


def cthulhu(msg):
    chat_id = msg['chat']['id']
    command = msg['text'].lower()
    print(command)
    tmp = dict()
    with open('fluxx-db.json', 'r') as db:
        tmp = json.load(db)

    if command in ('/start', '/help'):
        bot.sendMessage(chat_id, 'Scrivi solo il nome della carta')
    elif command == 'meta':
        pic_path = "cards/meta.png"
        bot.sendPhoto(chat_id, (pic_path, open(pic_path, 'rb')))
    elif command == 'rules':
        bot.sendDocument(chat_id, ("rules.pdf", open("rules.pdf", "rb")))
    elif command in tmp.keys():
        pic_path = "cards/" + tmp[command]
        bot.sendPhoto(chat_id, (pic_path, open(pic_path, 'rb')))
    else:
        bot.sendMessage(chat_id, "Qualcosa è andato storto, riprova.")


token = open('token.txt', 'r').readline().strip()
bot = telepot.Bot(token)
bot.message_loop(cthulhu)

while 1:
    time.sleep(10)
